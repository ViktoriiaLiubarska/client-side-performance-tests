/* @Author: Dmytro.Tyrtyshnyi@gmail.com */
package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Optional;
import java.util.concurrent.TimeUnit;


public class WebDriverHolder {
    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (!Optional.ofNullable(driver).isPresent()) {
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            return driver;
        } else {
            return driver;
        }
    }

    public static void setDriver(WebDriver driver) {
        WebDriverHolder.driver = driver;
    }
}
