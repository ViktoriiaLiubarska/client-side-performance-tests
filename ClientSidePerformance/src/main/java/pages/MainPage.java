/* @Author: Dmytro.Tyrtyshnyi@gmail.com */
package pages;

import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainPage extends BasePage {

    public MainPage() { super(); }

    public MainPage openMainPage() {
        wait.until(ExpectedConditions.elementToBeClickable(computersTitle));
        // Client-Side Performance measuring method
        perfNavigationTiming.writeToJson("MainPage");
        return this;
    }

}
