/* @Author: Dmytro.Tyrtyshnyi@gmail.com */
package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ComputersPage extends BasePage {
    public ComputersPage() {
        super();
    }

    @FindBy(xpath = "//div[@class='item-grid']")
    private WebElement computerCategories;

    public ComputersPage openComputersPage() {
        computersTitle.click();
        wait.until(ExpectedConditions.elementToBeClickable(computerCategories));
        waitUntilPageIsFullyLoaded(wait);
        perfNavigationTiming.writeToJson("ComputersPage");
        return this;
    }
}
