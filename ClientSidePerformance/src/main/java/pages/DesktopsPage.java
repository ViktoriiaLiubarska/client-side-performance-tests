/* @Author: Dmytro.Tyrtyshnyi@gmail.com */
package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;


import java.util.List;

public class DesktopsPage extends BasePage {
    public DesktopsPage() {
        super();
    }

    @FindBy(xpath = "//h2/a[@href='/desktops']")
    private WebElement desktopsPageButton;

    @FindBy(xpath = "//div[@class='details']//a")
    private List<WebElement> desktopProductsList;

    @FindBy(xpath = "//h1")
    private WebElement openedDesktopTitle;

    @FindBy(xpath = "//div[@class='overview']")
    private WebElement productDescriptionAndPricesButton;

    public DesktopsPage openDesktopsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(desktopsPageButton));
        desktopsPageButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(desktopProductsList.get(0)));
        waitUntilPageIsFullyLoaded(wait);
        perfNavigationTiming.writeToJson("DesktopsPage");
        return this;
    }

    public DesktopsPage openFirstDesktop() {
        String firstDesktopInTheListName = desktopProductsList.get(0).getText();
        desktopProductsList.get(0).click();
        wait.until(ExpectedConditions.elementToBeClickable(productDescriptionAndPricesButton));
        Assert.assertTrue(openedDesktopTitle.getText().contains(firstDesktopInTheListName), "Wrong desktop was opened");
        waitUntilPageIsFullyLoaded(wait);
        perfNavigationTiming.writeToJson("ProductPage");
        return this;
    }

}
